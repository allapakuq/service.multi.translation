import { NextFunction, Request, Response} from 'express';
import { HttpError } from 'http-errors';
import { ApiException, ApiResponse } from './commong.response';

export class ErrorHandling {

    static errorMiddleware(error: any, req: any, res: any, next: any) {
        const status = error.status || 500;
        const defaultCode = error.code || '001';
        const message = error.message || 'Error interno, comunicarse con el administrador.';
      
        var exception: ApiException = {
            code: defaultCode,
            description: message
        }
        var response: ApiResponse<string> = new ApiResponse();
        response.isValid = false;
        response.content = null;
        response.exceptions = [exception];
      
        res
          .status(status)
          .send(response);
      }

}