import express from 'express';
export abstract class CommonControllersConfig {
    constructor() {}

    validate<T>(req: express.Request): [boolean, T] {
        return [true, req.params as unknown as T]; //test
    }
}