import express from 'express';
export abstract class CommonRoutesConfig {
    app: express.Application;
    name: string;
    route: string;
    paths: Array<string> = [];

    constructor(app: express.Application, name: string, route: string) {
        this.app = app;
        this.name = name;
        this.route = route;
        this.configureRoutes();
    }

    getName() {
        return this.name;
    }
    getPaths() {
        return this.paths;
    }
    abstract configureRoutes(): express.Application;
    configurePath(endpoint: string): string{
        let path = `${this.route}${endpoint}`;
        this.paths.push(path);
        return path;
    }
}