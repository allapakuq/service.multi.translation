export class ApiResponse<T> {
    isValid: boolean = false;
    exceptions: Array<ApiException> = [];
    content: T = null;
 }

export class ApiException{
    code: string;
    description: string;

    constructor(code?: string, description?: string){
        this.code = code;
        this.description = description;
    }
    
 }