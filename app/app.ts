import 'dotenv/config'
import express from 'express';
import * as http from 'http';
import * as bodyparser from 'body-parser';
import * as winston from 'winston';
import * as expressWinston from 'express-winston';
import cors from 'cors';
import debug from 'debug';
import { TranslationRoutes } from './translates/translates.routes.config';
import { CommonRoutesConfig } from './common/common.routes.config';
import { ErrorHandling } from './common/commong.error.config';

require('dotenv').config();
process.setMaxListeners(0);

const app: express.Application = express();
const server: http.Server = http.createServer(app);
const hostname = process.env.HOST_HTTP;
const port = process.env.PORT_HTTP;
const routes: Array<CommonRoutesConfig> = [];
const debugLog: debug.IDebugger = debug('app');

app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());
app.use(cors());

// Add headers
app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    next();
});

console.log('Servidor HTTP iniciando : ' + new Date());

app.use(expressWinston.logger({
    transports: [
        new winston.transports.Console()
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    )
}));

routes.push(new TranslationRoutes(app));

app.use(expressWinston.errorLogger({
    transports: [
        new winston.transports.Console()
    ],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json()
    )
}));

app.get('/', (req: express.Request, res: express.Response) => {
    res.status(200).send(`Server running at http://${hostname}:${port}`)
});

server.listen(port, () => {
    console.log(`Server running at http://${hostname}:${port}`);
    routes.forEach((route: CommonRoutesConfig) => {
        console.log(`Routes configured for ${route.getName()}`);
        route.getPaths().forEach((path: string) => {
            console.log(`${path}`);
        });
    });
});

app.use(ErrorHandling.errorMiddleware);