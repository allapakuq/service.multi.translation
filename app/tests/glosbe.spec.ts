import { test, expect } from '@playwright/test';

test('glosbe test', async ({ page }) => {
  let fromLenguage = 'es';
  let toLengugage = 'qu';
  let text = 'gracias';

  await page.goto(`https://es.glosbe.com/${fromLenguage}/${toLengugage}/${text}`);
  const title = page.locator('.translations__list .translation__item .font-medium').first();
  await expect(title).toHaveText('Añay');
});