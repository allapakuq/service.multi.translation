import {CommonRoutesConfig} from '../common/common.routes.config';
import express from 'express';
import TranslatesController from './controllers/translates.controller';

export class TranslationRoutes extends CommonRoutesConfig {
    searchPath: string;
    
    constructor(app: express.Application) {
        super(app, 'TranslatesRoutes', '/translates');
    }

    configureRoutes() {
        this.searchPath = super.configurePath('/search');

        /*
            endpoint: /translates/search?lenguage=es&text=hola
        */
        this.app.get(this.searchPath, TranslatesController.search);
        
        return this.app;
    }
}