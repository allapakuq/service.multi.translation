import { SearchRequest } from '../dto/request/search.request';
import { ITranslates } from '../interfaces/translates.interface';
import translateScrap from '../scraps/glosbe';

class TranslatesService implements ITranslates {

    async search(request: SearchRequest) : Promise<string>{
        const translatedText = await translateScrap(request.from, request.to, request.text);
        return translatedText; //agregar logica
    }

}

export default new TranslatesService();