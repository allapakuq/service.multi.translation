import express from 'express';
import debug from 'debug';
import TranslatesService from '../services/translates.service';
import { SearchRequest } from "../dto/request/search.request";
import { CommonControllersConfig } from '../../common/common.controlles.config';
import { SearchResponse } from '../dto/response/search.response';
import { ApiException, ApiResponse } from '../../common/commong.response';

const log: debug.IDebugger = debug('app:users-controller');
class TranslatesController extends CommonControllersConfig{

    async search(req: express.Request, res: express.Response, next: express.NextFunction) {
        try {

            let response : ApiResponse<SearchResponse> = new ApiResponse();

            let from = req.query.from as string;
            let to = req.query.to as string;
            let text = req.query.text as string;

            let request : SearchRequest = {
                from: from,
                to: to,
                text: text
            };

            const translatedText = await TranslatesService.search(request);

            if(translatedText === ''){
                response.exceptions = [new ApiException('001', 'No se encontró traducción')]
            }else{
                let content : SearchResponse = {
                    from: from,
                    to: to,
                    translatedText: translatedText
                };
                response.isValid = true;
                response.content = content;
            }

            res.status(200).send(response);
            
        } catch (error) {
            next(error);
        }
    }
}

export default new TranslatesController();