export interface SearchRequest {
    from: string;
    to: string;
    text: string;
 }