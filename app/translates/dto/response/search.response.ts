export interface SearchResponse{
    from: string;
    to: string;
    translatedText: string;
 }