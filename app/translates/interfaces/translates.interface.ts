import { SearchRequest } from "../dto/request/search.request";

export interface ITranslates {
    search: (request: SearchRequest) => Promise<any>,
}