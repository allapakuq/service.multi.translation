const { chromium } = require("playwright-chromium");
const playwright = require('playwright');

const translateScrap = async (fromLenguage: string, toLengugage: string, text: string) : Promise<string> => {
    const browser = await chromium.launch({ headless: true });
    const timeout = 5000;
      try {
        var translateText = '';
        let page = await browser.newPage();
        page.setDefaultTimeout(timeout);
        let url = `https://es.glosbe.com/${fromLenguage}/${toLengugage}/${text}`;
        await page.goto(url, { waitUntil: "domcontentloaded" });
        var nodo = page.locator('.translations__list .translation__item .font-medium').first();
        if(!nodo){
            return translateText;
        }else{
            let nodecount = await nodo.count();
            if(nodecount <= 0){
                return translateText;
            }
            let resultNodeText = await nodo.evaluate(node => node.innerText, { timeout: timeout });
            if(resultNodeText == undefined || !resultNodeText || resultNodeText === ''){
                return translateText;
            }else{
                translateText = resultNodeText.trim();
            }
        }
        return translateText;
      }catch (error) {
        if (error.constructor.name === playwright.errors.TimeoutError.name){
            error.message = 'Tiempo de consulta excedido.'
        }
        throw error;
      }finally {
        await browser.close();
      }
  };
export default translateScrap;
