<p align="center">
  <a href="https://nodejs.org/es/">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/200px-Node.js_logo.svg.png" alt="Logo" width=72 height=72>
  </a>

  <h3 align="center">Multi Translation Service :speech_balloon: :robot:</h3>

  <p align="center">
    This multi-page http scraping service instantly translates words, phrases and web pages from Spanish to more languages and vice versa.
    <br>
    Base project made with much  :heart: . Contains express, scraping, patterns, and much more!
    <br>
    <br>
    <a href="https://jhon-coronel-bautista.atlassian.net/jira/software/projects/AQ/boards/1">Jira board</a>
    ·
    <a href="https://bitbucket.org/allapakuq/app.mobile.allapakuq/jira">Jira issues</a>
  </p>
</p>

## Table of contents

- [Getting Started](#getting-started)
- [Copyright and license](#copyright-and-license)

## Getting Started

The repository code is preloaded with some basic components like basic app architecture, app theme, constants and required dependencies to create a new project.

## How to Use 

**Step 1:**

Download or clone this repo by using the link below:

```
git clone https://Jhon-cb@bitbucket.org/allapakuq/service.multi.translation.git
```

**Step 2:**

Run commands in the following path: 

```
cd app
```

**Step 3:**

Go to project root and execute the following command in console to get the required dependencies: 

```
npm install
```


**Step 4:**

This project uses `nodemon`, execute the following command to run app:

```
npm run serve
```

Or you can run the program through visual code by pressing F5

## Copyright and License
 
The MIT License (MIT)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Copyright (c) 2022 Allapakuq

Enjoy :gem: